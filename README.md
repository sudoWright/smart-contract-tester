This is a series of standalone HTML pages that assist in allowing anyone to check the consistency of data between two smart contracts on different Ethereum-based (EVM) chains. The only requirement needed to run these tests is a network connection (no need for a wallet plugin) as it connects to public Ethereum nodes directly. It uses Next.js as a build environment to make designing the application more robust.

## Standalone testing
To use this tool with minimal dependencies, the compiled output of the build process (Next.js compiling the `.tsx` files down to `.html` files) can be opened in a web browser locally, without the need to host/serve the files. Download [the latest distribution package](https://gitlab.com/mooncatrescue/smart-contract-tester/-/jobs/artifacts/master/download?job=pages) from this repository, extract it and open the `public/index.html` file. From there you can navigate to the test you wish to run, and follow the prompts.

Alternatively, the static testing pages are hosted on GitLab pages for this repository: [click here](https://mooncatrescue.gitlab.io/smart-contract-tester/) to use those hosted files.

## Next.js environment testing
Using the standalone `.html` files requires some trust that the `.html` files in the archive are indeed generated from the source files in this repository. An alternative way to use the tool is to run the Next.js compilation yourself. A Docker configuration file is provided to make that possible on many different workstation configurations. If you have Docker installed, run `docker-compose up` and then navigate to [http://localhost:25650/mooncat-traits-goerli](http:localhost:25650/mooncat-traits-goerli) with your browser to see the testing file.
