import React from 'react'

export function sleep(delay: number): Promise<void> {
  return new Promise((resolve) => {
    window.setTimeout(() => {
      resolve()
    }, delay * 1000)
  })
}

// For a specific test result, show if it's a match or not
export function checkResult(expected: any, actual: any, label: String): JSX.Element {
  return expected == actual ? (
    <span className="test passed" key={label as React.Key}>
      {label} {actual.toString()}
    </span>
  ) : (
    <span className="test failed" key={label as React.Key}>
      {label} <span className="actual">{actual.toString()}</span> != <span className="expected">{expected.toString()}</span>
    </span>
  )
}

export function interleave(arr: Array<JSX.Element | string>, glue: string = ', '): Array<JSX.Element | String> {
  let formatted: Array<JSX.Element | string> = []
  arr.forEach((field, index) => {
    formatted.push(field)
    if (index < arr.length - 1) formatted.push(glue)
  })
  return formatted;
}