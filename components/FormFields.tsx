import React, { useState, useEffect, FC } from 'react'
import { interleave } from '../lib/util'

export type FormFieldChangeHandler = (newValue: string) => void
type FormFieldProps = {
  label: string
  className?: string
  value: string
  onChange: FormFieldChangeHandler
  onBlur?: (e: object) => void
}
/**
 * A generic input field wrapper, which grabs changed values from the vanilla HTML input element
 */
export const FormField: FC<FormFieldProps> = ({ label, className, value, onChange, onBlur = function () {} }) => {
  return (
    <label>
      {label + ' '}
      <input
        type="text"
        className={className}
        value={value}
        onChange={(e) => onChange(e.target.value)}
        onBlur={onBlur}
      />
    </label>
  )
}

export type MinMaxChangeHandler = (min: number, max: number) => void
type MinMaxProps = {
  label: string
  min: number
  max: number
  onChange: MinMaxChangeHandler
}
/**
 * A field grouping of a "min" and "max" field with specific outer bounds
 */
export const MinMaxFields: FC<MinMaxProps> = ({ label, min, max, onChange }) => {
  let [minValue, setMin] = useState(min)
  let [maxValue, setMax] = useState(max)

  // On key press in the field, just update the values, so the user can see what they're typing
  const handleMinUpdate: FormFieldChangeHandler = (val) => {
    setMin(parseInt(val))
  }
  const handleMaxUpdate: FormFieldChangeHandler = (val) => {
    setMax(parseInt(val))
  }

  // When the user is done entering text in the field, do validation checks
  const handleBlur = () => {
    let newMin = minValue
    let newMax = maxValue
    if (newMin < min) {
      newMin = min
    }
    if (newMin > max) {
      newMin = max
    }

    if (newMax < min) {
      newMax = min
    }
    if (newMax > max) {
      newMax = max
    }

    if (newMax < newMin) {
      newMax = newMin
    }
    setMin(newMin)
    setMax(newMax)
  }

  // Whenever min or max changes, notify upstream
  useEffect(() => {
    onChange(minValue, maxValue)
  }, [minValue, maxValue, onChange])

  return (
    <FormFieldGroup
      label={label}
      fields={[
        <FormField
          label="Min"
          key="min"
          className="num-entry"
          value={minValue.toString()}
          onChange={handleMinUpdate}
          onBlur={handleBlur}
        />,
        <FormField
          label="Max"
          key="max"
          className="num-entry"
          value={maxValue.toString()}
          onChange={handleMaxUpdate}
          onBlur={handleBlur}
        />,
      ]}
    />
  )
}

/**
 * A decorator component to have a row of form fields, prefixed by some header label
 */
type FormFieldGroupProps = {
  label: string
  fields: Array<JSX.Element>
}
export const FormFieldGroup: FC<FormFieldGroupProps> = ({ label, fields }) => {
  return (
    <>
      <span className="field-group-label">{label}</span>: {interleave(fields, ', ')}
    </>
  )
}
