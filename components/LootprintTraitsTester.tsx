import { ethers } from 'ethers'
import React, { useState, useRef, useEffect, useCallback, FC } from 'react'
import { MinMaxFields, MinMaxChangeHandler, FormField, FormFieldGroup } from '../components/FormFields'
import { sleep, interleave, checkResult } from '../lib/util'

type LootprintTraitsTesterProps = {
  lootprintAddress: string
  crossChainAddress: string
  crossChainName: string
  defaultMainChainRPC?: string
  defaultCrossChainRPC: string
}
/**
 * Component that handles setup of the initial test run, and then showing the results of the run as it processes
 */
export const LootprintTraitsTester: FC<LootprintTraitsTesterProps> = ({
  lootprintAddress,
  crossChainAddress,
  crossChainName,
  defaultMainChainRPC = 'https://eth-rpc.gateway.pokt.network',
  defaultCrossChainRPC,
}) => {
  let [log, setLog] = useState<Array<JSX.Element | String>>([])
  let logContainer = useRef<HTMLUListElement>(null)

  // Whenever 'log' updates, if the logContainer DOM element is present, scroll it to the bottom
  useEffect(() => {
    if (logContainer.current == null) return
    let box = logContainer.current
    if (box.scrollHeight - box.scrollTop - box.clientHeight > 125) return // User has back-scrolled; don't jump to bottom
    box.scrollTop = box.scrollHeight + 200
  }, [log])

  // Append a message to the end of the log
  function addLog(msg: JSX.Element | String) {
    setLog((oldLog) => [...oldLog, msg])
  }

  // Configuration for the test run
  let [lootprintIds, setLootprintIds] = useState({ min: 0, max: 25599 })
  let [mainChainRPC, setMainRPC] = useState(defaultMainChainRPC)
  let [testChainRPC, setTestRPC] = useState(defaultCrossChainRPC)

  const rawHandler: MinMaxChangeHandler = (min, max) => {
    setLootprintIds({ min, max })
  }
  const handleIdChange = useCallback(rawHandler, [])

  // Main logic handler; triggered by the user clicking the button to start the test
  async function runTest() {
    addLog('Connecting to blockchains...')
    let mainChainProvider = new ethers.providers.JsonRpcProvider(mainChainRPC)
    let testChainProvider = new ethers.providers.JsonRpcProvider(testChainRPC)

    // Create Ethers.js Contract objects for each of the contracts to be interacted with
    const LOOTPRINTS = new ethers.Contract(
      lootprintAddress,
      [
        'function Lootprints (uint256) external view returns ((uint16 index, address owner))',
        'function getDetails (uint256 lootprintId) external view returns (uint8 status, string memory class, uint8 bays, string memory colorName, string memory shipName, address tokenOwner, uint32 seed)',
      ],
      mainChainProvider
    )

    const LOOTPRINTS_CROSSCHAIN = new ethers.Contract(
      crossChainAddress,
      [
        'function getClassName (uint8 classId) external view returns (string memory)',
        'function getColorName (uint8 colorId) external view returns (string memory)',
        'function getDetails (uint256 lootprintId) external view returns (uint8 class, uint8 color, uint8 bays, string memory shipName, uint16 index, uint32 seed)',
      ],
      testChainProvider
    )

    // Fetch human-friendly names for different identifiers
    addLog('Fetching class names...')
    let CLASS_NAMES: Array<string> = []
    for (let i = 0; i < 4; i++) {
      CLASS_NAMES[i] = await LOOTPRINTS_CROSSCHAIN.getClassName(i)
    }

    addLog('Fetching color names...')
    let COLOR_NAMES: Array<string> = []
    for (let i = 0; i < 15; i++) {
      COLOR_NAMES[i] = await LOOTPRINTS_CROSSCHAIN.getColorName(i)
    }

    // Job runner for a single lootprint
    async function checkLootprint(lootprintId: number): Promise<JSX.Element> {
      // Trigger several interactions and wrap in a Promise.all to allow simultaneous queries
      let [originalTraits, originalStruct] = await Promise.all([
        LOOTPRINTS.getDetails(lootprintId),
        LOOTPRINTS.Lootprints(lootprintId),
      ])
      if (originalTraits.seed == 0) {
        // This lootprint was never minted; cross-chain contract should revert on a details query
        try {
          await LOOTPRINTS_CROSSCHAIN.getDetails(lootprintId)
          // Call succeeded? That's not correct
          return (
            <>
              Lootprint #{lootprintId}:{' '}
              <span className="test failed">
                <span className="actual">Non-existent lootprint has details</span>
              </span>
            </>
          )
        } catch (err) {
          console.log(err)
          // Call reverted; that's expected for a non-existent lootprint
          return (
            <>
              Lootprint #{lootprintId}: <span className="test passed">Lootprint was not minted</span>
            </>
          )
        }
      }

      // Test grouped metadata
      let crossTraits = await LOOTPRINTS_CROSSCHAIN.getDetails(lootprintId)

      let tests: Array<JSX.Element> = [
        checkResult(originalStruct.index, crossTraits.index, 'Index'),
        checkResult(originalTraits.seed, crossTraits.seed, 'Seed'),
        checkResult(originalTraits.class, CLASS_NAMES[crossTraits.class], 'Class'),
        checkResult(originalTraits.colorName, COLOR_NAMES[crossTraits.color], 'Color'),
        checkResult(originalTraits.bays, crossTraits.bays, 'Bays'),
        checkResult(originalTraits.shipName, crossTraits.shipName, 'Name'),
      ]
      return (
        <>
          Lootprint #{lootprintId}: {interleave(tests, ', ')}
        </>
      )
    }

    // Step through the desired MoonCat IDs, in batches of 3 simultaneous tests:
    addLog(`Checking from MoonCat #${lootprintIds.min} to #${lootprintIds.max}...`)
    let curIndex = lootprintIds.min
    let jobQueue: Array<Promise<JSX.Element>> = []
    while (curIndex <= lootprintIds.max) {
      // Fill the job queue
      while (jobQueue.length < 3 && curIndex <= lootprintIds.max) {
        jobQueue.push(checkLootprint(curIndex))
        curIndex++
      }

      // Process the job queue
      try {
        let rs = await Promise.all(jobQueue)
        rs.forEach((msg) => addLog(msg))
      } catch (err) {
        console.error(err)
        addLog('Error fetching data from Ethereum Node. Delaying before a retry...')
        await sleep(10)
        try {
          let rs = await Promise.all(jobQueue)
          rs.forEach((msg) => addLog(msg))
        } catch (err) {
          console.error(err)
          addLog('Error fetching data again; aborting.')
          return
        }
      }

      // Reset the job queue
      jobQueue = []
    }

    addLog('Done!')
  }

  if (log.length == 0) {
    return (
      <>
        <p>
          <MinMaxFields label="MoonCat IDs" min={0} max={25599} onChange={handleIdChange} />
        </p>
        <p>
          <FormFieldGroup
            label="Node RPC endpoints"
            fields={[
              <FormField key="main" label="Main" className="uri-entry" value={mainChainRPC} onChange={setMainRPC} />,
              <FormField
                key="test"
                label={crossChainName}
                className="uri-entry"
                value={testChainRPC}
                onChange={setTestRPC}
              />,
            ]}
          />
        </p>
        <p>
          <button onClick={runTest}>Start Test</button>
        </p>
      </>
    )
  } else {
    return (
      <ul className="log" ref={logContainer}>
        {log.map((msg, i) => (
          <li key={i}>{msg}</li>
        ))}
      </ul>
    )
  }
}
