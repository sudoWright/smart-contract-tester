import { ethers } from 'ethers'
import React, { useState, useRef, useEffect, useCallback, FC } from 'react'
import { MinMaxFields, MinMaxChangeHandler, FormField, FormFieldGroup } from './FormFields'
import { sleep, interleave, checkResult } from '../lib/util'

type MoonCatTraitsTesterProps = {
  moonCatRescueAddress: string
  moonCatTraitsAddress: string
  moonCatColorsAddress: string
  crossChainAddress: string
  crossChainName: string
  defaultMainChainRPC?: string
  defaultCrossChainRPC: string
}
/**
 * Component that handles setup of the initial test run, and then showing the results of the run as it processes
 */
export const MoonCatTraitsTester: FC<MoonCatTraitsTesterProps> = ({
  moonCatRescueAddress,
  moonCatTraitsAddress,
  moonCatColorsAddress,
  crossChainAddress,
  crossChainName,
  defaultMainChainRPC = 'https://eth-rpc.gateway.pokt.network',
  defaultCrossChainRPC,
}) => {
  let [log, setLog] = useState<Array<JSX.Element | String>>([])
  let logContainer = useRef<HTMLUListElement>(null)

  // Whenever 'log' updates, if the logContainer DOM element is present, scroll it to the bottom
  useEffect(() => {
    if (logContainer.current == null) return
    let box = logContainer.current
    if (box.scrollHeight - box.scrollTop - box.clientHeight > 125) return // User has back-scrolled; don't jump to bottom
    box.scrollTop = box.scrollHeight + 200
  }, [log])

  // Append a message to the end of the log
  function addLog(msg: JSX.Element | String) {
    setLog((oldLog) => [...oldLog, msg])
  }

  // Configuration for the test run
  let [moonCatIds, setMoonCatIds] = useState({ min: 0, max: 25439 })
  let [mainChainRPC, setMainRPC] = useState(defaultMainChainRPC)
  let [testChainRPC, setTestRPC] = useState(defaultCrossChainRPC)

  const rawHandler: MinMaxChangeHandler = (min, max) => {
    setMoonCatIds({ min, max })
  }
  const handleIdChange = useCallback(rawHandler, [])

  // Main logic handler; triggered by the user clicking the button to start the test
  async function runTest() {
    addLog('Connecting to blockchains...')
    let mainChainProvider = new ethers.providers.JsonRpcProvider(mainChainRPC)
    let testChainProvider = new ethers.providers.JsonRpcProvider(testChainRPC)

    // Create Ethers.js Contract objects for each of the contracts to be interacted with
    const MOONCAT_RESCUE = new ethers.Contract(
      moonCatRescueAddress,
      ['function rescueOrder (uint256 order) external view returns (bytes5 catId)'],
      mainChainProvider
    )

    const MOONCAT_TRAITS = new ethers.Contract(
      moonCatTraitsAddress,
      [
        'function traitsOf (uint256 rescueOrder) external view returns (bool genesis, bool pale, string memory facing, string memory expression, string memory pattern, string memory pose)',
      ],
      mainChainProvider
    )

    const MOONCAT_COLORS = new ethers.Contract(
      moonCatColorsAddress,
      ['function hueIntOf (uint256 rescueOrder) external view returns (uint16)'],
      mainChainProvider
    )
    const MOONCAT_CROSSCHAIN = new ethers.Contract(
      crossChainAddress,
      [
        'function catIdOf (uint256 rescueOrder) external view returns (bytes5 catId)',
        'function hueIntOf (uint256 rescueOrder) external view returns (uint16)',
        'function traitsOf (uint256 rescueOrder) external view returns (bool genesis, bool pale, string memory facing, string memory expression, string memory pattern, string memory pose)',
      ],
      testChainProvider
    )

    // Job runner for a single MoonCat
    async function checkMoonCat(rescueOrder: number): Promise<JSX.Element> {
      // Trigger several interactions and wrap in a Promise.all to allow simultaneous queries
      let [originalId, originalHue, originalTraits, crossChainId, crossChainHue, crossChainTraits] = await Promise.all([
        MOONCAT_RESCUE.rescueOrder(rescueOrder),
        MOONCAT_COLORS.hueIntOf(rescueOrder),
        MOONCAT_TRAITS.traitsOf(rescueOrder),
        MOONCAT_CROSSCHAIN.catIdOf(rescueOrder),
        MOONCAT_CROSSCHAIN.hueIntOf(rescueOrder),
        MOONCAT_CROSSCHAIN.traitsOf(rescueOrder),
      ])

      let tests: Array<JSX.Element> = [
        checkResult(originalId, crossChainId, 'ID'),
        checkResult(originalHue, crossChainHue, 'Color Hue'),
        checkResult(originalTraits.genesis, crossChainTraits.genesis, 'Is Genesis?'),
        checkResult(originalTraits.pale, crossChainTraits.pale, 'Is Pale?'),
        checkResult(originalTraits.facing, crossChainTraits.facing, 'Facing'),
        checkResult(originalTraits.expression, crossChainTraits.expression, 'Expression'),
        checkResult(originalTraits.pattern, crossChainTraits.pattern, 'Pattern'),
        checkResult(originalTraits.pose, crossChainTraits.pose, 'Pose'),
      ]
      return (
        <>
          MoonCat #{rescueOrder}: {interleave(tests, ',')},
        </>
      )
    }

    // Step through the desired MoonCat IDs, in batches of 3 simultaneous tests:
    addLog(`Checking from MoonCat #${moonCatIds.min} to #${moonCatIds.max}...`)
    let curIndex = moonCatIds.min
    let jobQueue: Array<Promise<JSX.Element>> = []
    while (curIndex <= moonCatIds.max) {
      // Fill the job queue
      while (jobQueue.length < 3 && curIndex <= moonCatIds.max) {
        jobQueue.push(checkMoonCat(curIndex))
        curIndex++
      }

      // Process the job queue
      try {
        let rs = await Promise.all(jobQueue)
        rs.forEach((msg) => addLog(msg))
      } catch (err) {
        console.error(err)
        addLog('Error fetching data from Ethereum Node. Delaying before a retry...')
        await sleep(10)
        try {
          let rs = await Promise.all(jobQueue)
          rs.forEach((msg) => addLog(msg))
        } catch (err) {
          console.error(err)
          addLog('Error fetching data again; aborting.')
          return
        }
      }

      // Reset the job queue
      jobQueue = []
    }

    addLog('Done!')
  }

  if (log.length == 0) {
    return (
      <>
        <p>
          <MinMaxFields label="MoonCat IDs" min={0} max={25439} onChange={handleIdChange} />
        </p>
        <p>
          <FormFieldGroup
            label="Node RPC endpoints"
            fields={[
              <FormField key="main" label="Main" className="uri-entry" value={mainChainRPC} onChange={setMainRPC} />,
              <FormField
                key="test"
                label={crossChainName}
                className="uri-entry"
                value={testChainRPC}
                onChange={setTestRPC}
              />,
            ]}
          />
        </p>
        <p>
          <button onClick={runTest}>Start Test</button>
        </p>
      </>
    )
  } else {
    return (
      <ul className="log" ref={logContainer}>
        {log.map((msg, i) => (
          <li key={i}>{msg}</li>
        ))}
      </ul>
    )
  }
}
