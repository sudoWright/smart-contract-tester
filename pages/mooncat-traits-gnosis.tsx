import type { NextPage } from 'next'
import Head from 'next/head'
import { MoonCatTraitsTester } from '../components/MoonCatTraitsTester'

// Configuration for the contracts to be tested
const MOONCATRESCUE_ADDRESS = '0x60cd862c9C687A9dE49aecdC3A99b74A4fc54aB6'
const MOONCAT_TRAITS_ADDRESS = '0x9330BbfBa0C8FdAf0D93717E4405a410a6103cC2'
const MOONCAT_COLORS_ADDRESS = '0x2fd7E0c38243eA15700F45cfc38A7a7f66df1deC'

const CROSS_CHAIN_ADDRESS = '0xf00e9cF6a96dFAd869f676022F49761632A3aA2C'
const CROSS_CHAIN_NAME = 'Gnosis'

/**
 * Decorator component to put a preamble before the test runner on the page
 */
const DeployTester: NextPage = () => {
  return (
    <section>
      <Head>
        <title>MoonCatRescue Cross-chain Tester</title>
        <meta name="description" content="Verify the on-chain trait data for MoonCat assets are the same cross-chain" />
        <link rel="icon" href="./favicon.ico" />
      </Head>
      <h1>MoonCatRescue Cross-chain Tester</h1>
      <ul>
        <li>
          Original Contract: <code>{MOONCATRESCUE_ADDRESS}</code> (Mainnet)
        </li>
        <li>
          On-Chain Traits: <code>{MOONCAT_TRAITS_ADDRESS}</code> (Mainnet)
        </li>
        <li>
          On-Chain Colors: <code>{MOONCAT_COLORS_ADDRESS}</code> (Mainnet)
        </li>
        <li className="highlighted">
          Cross-Chain Metadata: <code>{CROSS_CHAIN_ADDRESS}</code> ({CROSS_CHAIN_NAME})
        </li>
      </ul>
      <p>
        The <a href={`https://etherscan.io/address/${MOONCATRESCUE_ADDRESS}`}>original MoonCatRescue contract</a>{' '}
        contains the canonical information for which MoonCat IDs were rescued in which order. The{' '}
        <a href={`https://etherscan.io/address/${MOONCAT_TRAITS_ADDRESS}`}>MoonCatTraits contract</a> contains on-chain
        logic for parsing a MoonCat&rsquo;s ID into individual traits, and the{' '}
        <a href={`https://etherscan.io/address/${MOONCAT_COLORS_ADDRESS}`}>MoonCatColors contract</a> contains on-chain
        lookups for what color (hue) each MoonCat ID is. The cross-chain metadata contract <em>should</em> perfectly
        replicate the data from the first two. This tester connects to both chains and verifies the information is the
        same, for all MoonCats in the specified range.
      </p>
      <MoonCatTraitsTester
        moonCatRescueAddress={MOONCATRESCUE_ADDRESS}
        moonCatTraitsAddress={MOONCAT_TRAITS_ADDRESS}
        moonCatColorsAddress={MOONCAT_COLORS_ADDRESS}
        crossChainAddress={CROSS_CHAIN_ADDRESS}
        crossChainName={CROSS_CHAIN_NAME}
        defaultCrossChainRPC="https://rpc.gnosischain.com"
      />
    </section>
  )
}

export default DeployTester
