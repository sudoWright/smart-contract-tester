import type { NextPage } from 'next'
import Head from 'next/head'

const Home: NextPage = () => {
  return (
    <section>
      <Head>
        <title>Smart Contract Testing tools</title>
        <link rel="icon" href="./favicon.ico" />
      </Head>
      <p>This is a series of standalone HTML pages that assist in allowing anyone to check the consistency of data between two smart contracts on different Ethereum-based (EVM) chains. Each of the links below leads to a testing tool for that bit of metadata, on that specific chain. The only requirement needed to run these tests is a network connection (no need for a wallet plugin) as it connects to public Ethereum nodes directly. Check <a href="https://gitlab.com/mooncatrescue/smart-contract-tester">the source repository</a> to keep up-to-date with new/updated tests.</p>
      <ul>
        <li><a href="./mooncat-traits-goerli">MoonCat Traits (Goerli)</a></li>
        <li><a href="./mooncat-traits-gnosis">MoonCat Traits (Gnosis)</a></li>
        <li><a href="./mooncat-traits-matic">MoonCat Traits (Matic)</a></li>
        <li><a href="./mooncat-traits-optimism">MoonCat Traits (Optimism)</a></li>
        <li><a href="./lootprint-traits-goerli">Lootprint Traits (Goerli)</a></li>
        <li><a href="./lootprint-traits-gnosis">Lootprint Traits (Gnosis)</a></li>
        <li><a href="./lootprint-traits-matic">Lootprint Traits (Matic)</a></li>
        <li><a href="./lootprint-traits-optimism">Lootprint Traits (Optimism)</a></li>
      </ul>
    </section>
  )
}

export default Home
