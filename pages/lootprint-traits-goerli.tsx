import type { NextPage } from 'next'
import Head from 'next/head'
import { LootprintTraitsTester } from '../components/LootprintTraitsTester'

// Configuration for the contracts to be tested
const LOOTPRINT_ADDRESS = '0x1e9385eE28c5C7d33F3472f732Fb08CE3ceBce1F'
const CROSS_CHAIN_ADDRESS = '0x7F994F20a5C894a96CA7185640Ed1fC2c9c3AfAa'
const CROSS_CHAIN_NAME = 'Goerli'

/**
 * Decorator component to put a preamble before the test runner on the page
 */
const DeployTester: NextPage = () => {
  return (
    <section>
      <Head>
        <title>Lootprints Cross-chain Tester</title>
        <meta name="description" content="Verify the on-chain trait data for MoonCat assets are the same cross-chain" />
        <link rel="icon" href="./favicon.ico" />
      </Head>
      <h1>Lootprints (for MoonCats) Cross-chain Tester</h1>
      <ul>
        <li>
          Original Contract: <code>{LOOTPRINT_ADDRESS}</code> (Mainnet)
        </li>
        <li className="highlighted">
          Cross-Chain Metadata: <code>{CROSS_CHAIN_ADDRESS}</code> ({CROSS_CHAIN_NAME})
        </li>
      </ul>
      <p>
        The <a href={`https://etherscan.io/address/${LOOTPRINT_ADDRESS}`}>original lootprints contract</a> contains the
        canonical information for information about each lootprint. Specifically, its Name, Color, Classification, Bay
        count, Index (mint order) and Seed (random data used to derive the other traits) The cross-chain metadata
        contract <em>should</em> perfectly replicate that data. This tester connects to both chains and verifies the
        information is the same, for all lootprints in the specified range.
      </p>
      <LootprintTraitsTester
        lootprintAddress={LOOTPRINT_ADDRESS}
        crossChainAddress={CROSS_CHAIN_ADDRESS}
        crossChainName={CROSS_CHAIN_NAME}
        defaultCrossChainRPC='https://rpc.ankr.com/eth_goerli'
      />
    </section>
  )
}

export default DeployTester
